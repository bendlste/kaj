
let openNav = document.querySelector(".open-slide > a");
let closeNav = document.querySelector(".btn-close");
openNav.addEventListener("click", openSlideMenu);
closeNav.addEventListener("click", closeSlideMenu);

function openSlideMenu() {
    document.querySelector("#side-menu").style.width = "250px";
    document.querySelector("#main").style.marginLeft = "250px";
}

function closeSlideMenu() {
    document.querySelector("#side-menu").style.width = "0";
    document.querySelector("#main").style.marginLeft = "0";
}