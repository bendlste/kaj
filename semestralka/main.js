class Main {
    constructor(gameLength) {
        this.mainButton = document.querySelector("#home");
        this.leaderboardsButton = document.querySelector("#leaderboards");
        this.rulesButton = document.querySelector("#rules");
        this.aboutButton = document.querySelector("#about");
        this.game = new Game(gameLength);
        this.addListeners()
    }

    addListeners() {
        this.mainButton.addEventListener("click", this.renderMainScreen.bind(this));
        this.leaderboardsButton.addEventListener("click", this.renderLeaderboardsScreen);
        this.rulesButton.addEventListener("click", this.renderRulesScreen);
        this.aboutButton.addEventListener("click", this.renderAboutScreen);
        document.querySelector(".new-game").addEventListener("click", () => {
            this.game.removeEventListeners();
            this.game = new Game(this.game.size);
            document.onkeyup = null;
            document.querySelector("canvas").classList.remove("game-over");
            this.screen = new Screen(this.game);
        });
        window.onpopstate = () => {
            switch (location.hash) {
                case "#home":
                    this.renderMainScreen();
                    break;
                case "#leaderboards":
                    this.renderLeaderboardsScreen();
                    break;
                case "#rules":
                    this.renderRulesScreen();
                    break;
                case "#about":
                    this.renderAboutScreen();
                    break;
                case "#nav":
                    history.back();
                    break;
            }
        }
    }

    renderMainScreen() {
        this.screen = new Screen(this.game);
        document.querySelector("#main").classList.remove("hidden");
        document.querySelector(".leaderboards").classList.add("hidden");
        document.querySelector(".rules").classList.add("hidden");
        document.querySelector(".about").classList.add("hidden");
    }

    renderLeaderboardsScreen() {
        document.querySelector("#main").classList.add("hidden");
        let div = document.querySelector(".leaderboards");
        document.querySelector(".rules").classList.add("hidden");
        document.querySelector(".about").classList.add("hidden");
        div.classList.remove("hidden");
        div.innerHTML = "";
        (function renderScores() {
            let scoreBoard = localStorage.scoreBoard == null ? [] : JSON.parse(localStorage.scoreBoard);
            if (scoreBoard.length === 0) {
                let spanEl = document.createElement("p");
                spanEl.innerText = "Nothing here yet. Play a game and be the first to get your name here.";
                div.append(spanEl);
                let imgEl = document.createElement("img");
                imgEl.src = "resources/do_it.jpg";
                div.append(imgEl);
            } else {
                let divElement = document.createElement("div");
                divElement.classList.add("score");

                for (let i = 0; i < scoreBoard.length; i++) {
                    let parent = document.createElement("div");
                    let position = document.createElement("span");
                    position.innerText = i + 1;
                    let name = document.createElement("span");
                    name.innerText = scoreBoard[i].name;
                    let score = document.createElement("span");
                    score.innerText = scoreBoard[i].score;
                    let city = document.createElement("span");
                    city.innerText = scoreBoard[i].city;
                    parent.appendChild(position);
                    parent.appendChild(name);
                    parent.appendChild(score);
                    parent.appendChild(city);
                    divElement.appendChild(parent);
                }
                div.appendChild(divElement);
            }
        })();
    }

    renderRulesScreen() {
        document.querySelector("#main").classList.add("hidden");
        let div = document.querySelector(".rules");
        document.querySelector(".leaderboards").classList.add("hidden");
        document.querySelector(".about").classList.add("hidden");
        div.classList.remove("hidden");
        div.innerHTML = "<p>Rules:</p>";
        let ulEl = document.createElement("ul");
        let liEl1 = document.createElement("li");
        liEl1.innerText = "Do not cheat.";
        let liEl2 = document.createElement("li");
        liEl2.innerText = "Do not be lazy. Finish what you started.";
        let liEl3 = document.createElement("li");
        liEl3.innerText = "";
        ulEl.appendChild(liEl1);
        ulEl.appendChild(liEl2);
        ulEl.appendChild(liEl3);
        div.appendChild(ulEl);
    }

    renderAboutScreen() {
        document.querySelector("#main").classList.add("hidden");
        let div = document.querySelector(".about");
        document.querySelector(".leaderboards").classList.add("hidden");
        document.querySelector(".rules").classList.add("hidden");
        div.classList.remove("hidden");
        div.innerHTML = "<p>Created by Štěpán Bendl as a KAJ semestral work.</p>";
    }
}

class Game {
    constructor(size) {
        this.player = new Player();
        this.size = size;
        this.tiles = [];
        this.generateArray(size);
        this.addListeners();
    }

    generateArray(length) {
        for (let i = 0; i < length; i++) {
            this.tiles[i] = new Array(length);
            for (let j = 0; j < length; j++) {
                this.tiles[i][j] = 0;
            }
        }
    }

    generateValue() {
        return Math.floor(Math.random() * 2) === 0;
    }

    getFreePositions() {
        let freePositions = [];
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                if (this.tiles[i][j] === 0) {
                    freePositions.push([i, j]);
                }
            }
        }
        return freePositions;
    }

    addNumberToArray(random) {
        let number = random ? 2 : 4;
        let freePositions = this.getFreePositions();
        if (freePositions.length === 0) {
            return;
        }
        let position = freePositions[Math.floor(Math.random() * freePositions.length)];
        this.tiles[position[0]][position[1]] = number;
    }

    checkGameOver() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                let tile = this.tiles[i][j];
                if (tile === 0) {
                    return false;
                }
                if (i === 0) {
                    if (j === 0) {
                        if (tile === this.tiles[i][j + 1] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    } else if (j === this.size - 1) {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    } else {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i][j + 1] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    }
                } else if (i === this.size - 1) {
                    if (j === 0) {
                        if (tile === this.tiles[i][j + 1] || tile === this.tiles[i - 1][j]) {
                            return false;
                        }
                    } else if (j === this.size - 1) {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i - 1][j]) {
                            return false;
                        }
                    } else {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i][j + 1] || tile === this.tiles[i - 1][j]) {
                            return false;
                        }
                    }
                } else if (j === 0) {
                    if (i === 0) {
                        if (tile === this.tiles[i][j + 1] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    } else if (i === this.size - 1) {
                        if (tile === this.tiles[i][j + 1] || tile === this.tiles[i - 1][j]) {
                            return false;
                        }
                    } else {
                        if (tile === this.tiles[i][j + 1] || tile === this.tiles[i - 1][j] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    }
                } else if (j === this.size - 1) {
                    if (i === 0) {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    } else if (i === this.size - 1) {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i - 1][j]) {
                            return false;
                        }
                    } else {
                        if (tile === this.tiles[i][j - 1] || tile === this.tiles[i - 1][j] || tile === this.tiles[i + 1][j]) {
                            return false;
                        }
                    }
                } else {
                    if (tile === this.tiles[i][j - 1] || tile === this.tiles[i][j + 1] ||
                        tile === this.tiles[i - 1][j] || tile === this.tiles[i + 1][j]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    removeEventListeners() {
        document.removeEventListener("keyup", e => {
            e.preventDefault();
            switch (e.key) {
                case "A":
                case "ArrowLeft":
                case "a":
                    this.moveLeft();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "S":
                case "ArrowDown":
                case "s":
                    this.moveDown();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "D":
                case "ArrowRight":
                case "d":
                    this.moveRight();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "W":
                case "ArrowUp":
                case "w":
                    this.moveUp();
                    this.addNumberToArray(this.generateValue());
                    break;
            }
            this.checkGameOver();
        });
    }

    addListeners() {
        document.addEventListener("keyup", e => {
            e.preventDefault();
            switch (e.key) {
                case "A":
                case "ArrowLeft":
                case "a":
                    this.moveLeft();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "S":
                case "ArrowDown":
                case "s":
                    this.moveDown();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "D":
                case "ArrowRight":
                case "d":
                    this.moveRight();
                    this.addNumberToArray(this.generateValue());
                    break;
                case "W":
                case "ArrowUp":
                case "w":
                    this.moveUp();
                    this.addNumberToArray(this.generateValue());
                    break;
            }
            if (this.checkGameOver()) {
                document.querySelector("canvas").classList.add("game-over");
                new Audio("resources/game_over.mp3").play();
                let name = window.prompt("Game over. Enter your name to save your score.");
                saveToLeaderboards(name, this.player.score);
                this.generateArray(this.size);
            }
        });
    }

    moveRight() {
        for (let i = 0; i < this.size; i++) {
            for (let j = this.size - 2; j >= 0; j--) {
                // Align tiles to the right and merge them if possible
                if (this.tiles[i][j] !== 0) {
                    for (let k = j; k + 1 < this.size; k++) {
                        // Align
                        if (this.tiles[i][k + 1] === 0) {
                            this.tiles[i][k + 1] = this.tiles[i][k];
                            this.tiles[i][k] = 0;
                        }
                        // Merge
                        if (this.tiles[i][k] === this.tiles[i][k + 1]) {
                            this.player.score += this.tiles[i][k];
                            this.tiles[i][k + 1] *= 2;
                            this.tiles[i][k] = 0;
                        }
                    }
                }

            }
        }
    }

    moveLeft() {
        for (let i = 0; i < this.size; i++) {
            for (let j = 1; j < this.size; j++) {
                // Align tiles to the left and merge them if possible
                if (this.tiles[i][j] !== 0) {
                    for (let k = j; k - 1 >= 0; k--) {
                        // Align
                        if (this.tiles[i][k - 1] === 0) {
                            this.tiles[i][k - 1] = this.tiles[i][k];
                            this.tiles[i][k] = 0;
                        }
                        // Merge
                        if (this.tiles[i][k] === this.tiles[i][k - 1]) {
                            this.player.score += this.tiles[i][k];
                            this.tiles[i][k - 1] *= 2;
                            this.tiles[i][k] = 0;
                        }
                    }
                }
            }
        }
    }

    moveDown() {
        for (let i = this.size - 2; i >= 0; i--) {
            for (let j = 0; j < this.size; j++) {
                // Align tiles to the left and merge them if possible
                if (this.tiles[i][j] !== 0) {
                    for (let k = i; k + 1 < this.size; k++) {
                        // Align
                        if (this.tiles[k + 1][j] === 0) {
                            this.tiles[k + 1][j] = this.tiles[k][j];
                            this.tiles[k][j] = 0;
                        }
                        // Merge
                        if (this.tiles[k][j] === this.tiles[k + 1][j]) {
                            this.player.score += this.tiles[k][j];
                            this.tiles[k + 1][j] *= 2;
                            this.tiles[k][j] = 0;
                        }
                    }
                }
            }
        }
    }

    moveUp() {
        for (let i = 1; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                // Align tiles to the left and merge them if possible
                if (this.tiles[i][j] !== 0) {
                    for (let k = i; k - 1 >= 0; k--) {
                        // Align
                        if (this.tiles[k - 1][j] === 0) {
                            this.tiles[k - 1][j] = this.tiles[k][j];
                            this.tiles[k][j] = 0;
                        }
                        // Merge
                        if (this.tiles[k][j] === this.tiles[k - 1][j]) {
                            this.player.score += this.tiles[k][j];
                            this.tiles[k - 1][j] *= 2;
                            this.tiles[k][j] = 0;
                        }
                    }
                }
            }
        }
    }
}

class Screen {
    constructor(game) {
        this.canvas = document.querySelector("canvas");
        this.ctx = this.canvas.getContext("2d");
        this.lineWidth = 10;
        this.canvasWidth = this.canvas.width - this.lineWidth;
        this.canvasHeight = this.canvas.height - this.lineWidth;
        this.backgroundColor = "#cdc0b4";
        this.lineColor = "#bbada0";
        this.tileSize = (this.canvas.width - (game.size + 1) * this.lineWidth) / game.size;
        this.game = game;
        this.drawGameBoard();
        document.onkeyup = () => this.drawGameBoard();
    }

    drawGameBoard() {
        this.ctx.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.ctx.fillStyle = this.backgroundColor;
        this.ctx.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.ctx.fillStyle = this.lineColor;
        for (let i = 0; i < this.game.size; i++) {
            this.ctx.fillRect(i * this.canvasWidth / this.game.size, 0, this.lineWidth, this.canvasHeight);
            this.ctx.fillRect(0, i * this.canvasHeight / this.game.size, this.canvasWidth, this.lineWidth);
        }
        this.ctx.fillRect(this.canvasWidth, 0, this.lineWidth, this.canvasHeight);
        this.ctx.fillRect(0, this.canvasHeight, this.canvasWidth + 10, this.lineWidth);

        this.drawTiles()

        this.renderScore();
    }

    drawTiles() {
        for (let i = 0; i < this.game.size; i++) {
            for (let j = 0; j < this.game.size; j++) {
                this.drawTile(this.game.tiles[i][j], j, i);
            }
        }
    }

    drawTile(number, x, y) {
        if (!number) return;
        let color = "#212121";
        switch (number) {
            case 2:
                color = "#eee4da";
                break;
            case 4:
                color = "#ede0c8";
                break;
            case 8:
                color = "#f2b179";
                break;
            case 16:
                color = "#f59563";
                break;
            case 32:
                color = "#f67c5f";
                break;
            case 64:
                color = "#f65e3b";
                break;
            case 128:
                color = "#edcf72";
                break;
            case 256:
                color = "#edcc61";
                break;
            case 512:
                color = "#edc850";
                break;
            case 1024:
                color = "#edc53f";
                break;
            case 2048:
                color = "#edc22e";
                break;
            case 4096:
                color = "#8bc34a";
                break;
        }
        let startX = x * (this.tileSize + this.lineWidth) + this.lineWidth;
        let startY = y * (this.tileSize + this.lineWidth) + this.lineWidth;

        this.ctx.fillStyle = color;
        this.ctx.fillRect(startX, startY, this.tileSize, this.tileSize);
        this.ctx.fillStyle = "#000";
        if (number > 4096) this.ctx.fillStyle = "#FFF";
        this.ctx.font = '24px serif';
        this.ctx.textAlign = 'center';
        this.ctx.fillText(number, startX + this.tileSize / 2, startY + this.tileSize / 2 + 10, this.tileSize);

    }

    renderScore() {
        document.querySelector("#score").innerHTML = `Score: ${this.game.player.score}`;
    }

}

class Player {
    constructor() {
        this.score = 0;
    }
}


function compare(a, b) {
    if (a.score > b.score) {
        return -1;
    }
    if (a.score < b.score) {
        return 1;
    }
    return 0;
}


// Helper functions
function saveToLeaderboards(name, score) {
    let scoreBoard = localStorage.scoreBoard == null ? [] : JSON.parse(localStorage.scoreBoard);
    scoreBoard.push({name: name, score: score, city: city});
    scoreBoard.sort(this.compare);
    localStorage.scoreBoard = JSON.stringify(scoreBoard);
}

let city = "";

async function success(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    if(navigator.onLine) {
        let responseFull = await fetch(`http://open.mapquestapi.com/geocoding/v1/reverse?key=7KR4mrTtOZO2t75RYIAHAULSbAfl26Qq&location=${latitude},${longitude}`);
        let response = await responseFull.json();
        city = response.results[0].locations[0].adminArea5;
    }
}

function error() {
    return "Not available";
}

navigator.geolocation.getCurrentPosition(success, error);

// History API


let m = new Main(4);
m.game.tiles[0][1] = 8192;
m.game.tiles[1][3] = 16348;
m.game.tiles[1][2] = 4096;
m.game.tiles[2][3] = 2048;